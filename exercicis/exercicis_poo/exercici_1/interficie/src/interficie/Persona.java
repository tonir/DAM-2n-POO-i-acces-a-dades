package interficie;

import java.util.Comparator;

public class Persona implements Comparable<Persona> {

	private float pes;
	private int edat;
	private float alcada;

	public float getPes() {
		return pes;
	}

	public void setPes(float pes) {
		this.pes = pes;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public float getAlcada() {
		return alcada;
	}

	public void setAlcada(float d) {
		this.alcada = d;
	}

	public static final Comparator<Persona> COMPARA_EDAT = new Comparator<Persona>(){
		
		@Override
		public int compare(Persona p1, Persona p2) {
			// TODO Auto-generated method stub
		
				if (p1.getEdat() > p2.getEdat()){
					
					return 1;
				}else if (p1.getEdat() < p2.getEdat()){
					
					return -1;
				}else{
			
					return 0;	
				}
		}

		
		
		
		public int compareTo(Persona arg0) {
		// TODO Auto-generated method stub
		return 0;
	}};

	@Override
	public int compareTo(Persona o) {
		// TODO Auto-generated method stub
		return 0;
	}}