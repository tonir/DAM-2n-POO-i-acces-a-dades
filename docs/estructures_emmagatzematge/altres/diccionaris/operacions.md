#### Operacions sobre diccionaris

Per **afegir** una nova parella de clau-valor, simplement:

`d.put(``"Ciutat",     "Sabadell");`

A un diccionari no hi poden haver claus repetides. Cal anar amb compte,
perquè si ciutat ja hagués existit hauriem sobreescrit el seu valor.

Per **esborrar una clau** d'un diccionari:

`d.remove("Ciutat");`

Aquest mètode, a més, retorna el valor associat a la clau eliminada. Si
el diccionari no conté la clau especificada, es retorna *null*.

Per **esborrar** totes les parelles de clau-valor del diccionari:

`d.clear();`

Per **comprovar** si **una clau** ja existeix al diccionari:

`d.containsKey("Ciutat");`

Retornarà *True* si la clau és al diccionari *d*.

Per **obtenir el valor** associat a una clau:

`d.get(clau);`

Ens retorna el valor associat a una clau i si no la troba, retorna
*null*.

Per saber la **quantitat d'elements** que té el diccionari:

`d.size();`

Per saber **totes les claus o valors** que hi ha en un diccionari:

`d.keySet();` retorna un conjunt amb totes les claus que hi
ha. L'ús d'aquest conjunt és molt interessant, perquè es manté
actualitzat amb els canvis que es fan al diccionari, i el diccionari
també incorpora les supressions que fem des del conjunt.

`d.values();` retorna una col·lecció amb tots els valors que
hi ha al diccionari. Igual que en el cas anterior, els canvis que es
realitzin sobre el diccionari es reflecteixen automàticament a la
col·lecció que hem obtingut.

***Exercici***: per què *keySet* retorna un conjunt i *values* retorna una
col·lecció?

Per **recórrer** tots els elements d'un diccionari:

```java
Set<String> claus = d.keySet();
for (String clau : claus)
    System.out.println(clau+": "+d.get(clau));
```

O també:

```java
Set<Entry<String, Integer>> info = d.entrySet();
for (Entry<String, Integer> dada : info)
    System.out.println(dada.getKey()+": "+dada.getValue());
```
